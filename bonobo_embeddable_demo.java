    // HelloStringifiedClient.java, stringified object reference version

import java.io.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;

import gnome.MonkeyBeans.*;
import gnome.OAF.*;
import gnome.Bonobo.*;

public class bonobo_embeddable_demo
{
    static ORB orb = null;
    
    public static org.omg.CORBA.Object ask_for_object ()
	throws Exception
	{
	    ObjectBrowser browser = new gnome.MonkeyBeans.OAF.ObjectBrowser (orb);
	    String iid = browser.get_iid (new String[] {
		"IDL:Bonobo/Embeddable:1.0"
	    });
	    
	    return browser.activate_from_id (iid);
	}

    static void run_remote_test (org.omg.CORBA.Object obj) throws Exception
	{
	    if (obj == null)
	    {
		System.err.println ("Activation failed");
		return;
	    }
	    
	    /* Narrow the passed object reference */
	    System.out.println ("Creating an embeddable from the object");	    
	    Unknown remote_obj = UnknownHelper.narrow (obj);
	    Embeddable embeddable =
		EmbeddableHelper.narrow (remote_obj.query_interface
					 ("IDL:Bonobo/Embeddable:1.0"));
	    // We don't need the Unknown anymore (QI ref()'s its
	    // returned value)
	    remote_obj.unref ();

	    // Create the container-side objects
	    System.out.println ("Creating utility objects for a view");
	    gnome.MonkeyBeans.App window = new gnome.MonkeyBeans.App ("teszt");

	    ContainerImpl container = new ContainerImpl (orb);
	    UIHandlerImpl uihandler = new UIHandlerImpl (orb, window);
	    ClientSiteImpl clientsite = new ClientSiteImpl (orb, container);

	    clientsite.bind_embeddable (embeddable);

	    // Create a new view for the embeddable associated with
	    // the clientsite
	    System.out.println ("Creating a view of the embeddable");
	    ViewFrameImpl frame = clientsite.new_view (uihandler);

	    View view = frame.get_view ();
	    gnome.MonkeyBeans.Control view_widget = new gnome.MonkeyBeans.Control (view);
	    view.activate (true);
	    
	    System.out.println ("Getting view properties");
	    PropertyBag bag = view.get_property_bag ();
	    if (bag != null)
	    {
		String[] prop_list = bag.get_property_names ();
		for (int i = 0; i < prop_list.length; i++)
		{
		    System.out.println ("PROP\t" + prop_list[i]);
		}
		bag.unref ();
	    }

	    window.add (view_widget);
	    window.setSize (300, 300);
	    window.setLocation (100, 100);
	    window.show ();

	    System.in.read ();

	    // Clean up
	    System.out.println ("Unrefing objects");	    

	    frame.unref ();      // -> View
	    container.unref (); // -> ClientSite -> Embeddable
	    uihandler.unref ();

	    System.exit (0);
	}

    
    public static void main(String args[])
	{
	    try{
		// create and initialize the ORB
		orb = ORB.init (args, null);
		POA rootPOA = (POA) orb.resolve_initial_references("RootPOA");
		POAManager manager = rootPOA.the_POAManager();
		manager.activate ();
		
		run_remote_test (ask_for_object ());

		System.exit (0);
		
	    } catch (Exception e) {
		System.err.println("ERROR : " + e) ;
		e.printStackTrace(System.err);
	    }
	}
}
