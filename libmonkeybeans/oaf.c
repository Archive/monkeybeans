/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/* gnome.MonkeyBeans.OAF.OAF */
#include "gnome_MonkeyBeans_OAF_OAF.h"

/* void gnome.MonkeyBeans.OAF.OAF::print_to_fd (int fd, String text) */
JNIEXPORT void JNICALL Java_gnome_MonkeyBeans_OAF_OAF_print_1to_1fd (JNIEnv *env,
								     jclass this,
								     jint fd,
								     jstring text)
{
    
    const char* text_chars = (*env)->GetStringUTFChars(env, text, NULL);
    FILE *outfile = fdopen (fd, "w");

    fprintf (outfile, "%s\n", text_chars);
    fclose (outfile);
    
    (*env)->ReleaseStringUTFChars (env, text, text_chars);
}
