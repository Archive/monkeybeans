/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/* gnome.MonkeyBeans.Socket */
#include "gnome_MonkeyBeans_Socket.h"

#include "jawt_md.h"

/* int gnome.MonkeyBeans.Socket::getX11Drawable () */
JNIEXPORT jint JNICALL Java_gnome_MonkeyBeans_Socket_getX11Drawable (JNIEnv *env,
								     jobject this)
{
    JAWT awt;
    JAWT_DrawingSurface* ds;
    JAWT_DrawingSurfaceInfo* dsi;
    JAWT_X11DrawingSurfaceInfo* dsi_x11;
    jint result;
    
    /* Get the AWT */
    awt.version = JAWT_VERSION_1_3;
    if (JAWT_GetAWT(env, &awt) == JNI_FALSE) {
        printf("AWT Not found\n");
        return -1;
    }

    /* Get the drawing surface */
    ds = awt.GetDrawingSurface(env, this);
    if (ds == NULL) {
        printf("NULL drawing surface\n");
        return -1;
    }

    /* Get the drawing surface info */
    dsi = ds->GetDrawingSurfaceInfo(ds);
    if (dsi == NULL) {
	printf("Error getting surface info\n");
        awt.FreeDrawingSurface(ds);
        return -1;
    }

    /* Get the platform-specific drawing info */
    dsi_x11 = (JAWT_X11DrawingSurfaceInfo*)dsi->platformInfo;

    /* Get the actual result */
    result = dsi_x11->drawable;
    
    /* Free the drawing surface info */
    ds->FreeDrawingSurfaceInfo(dsi);
   /* Free the drawing surface */
    awt.FreeDrawingSurface(ds);

    return result;
}
