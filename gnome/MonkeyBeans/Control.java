/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.MonkeyBeans;

public class Control extends Socket
{
    boolean setup_done = false;
    gnome.Bonobo.Control ctrl;
    
    public void paint (java.awt.Graphics g)
	{
	    if (setup_done)
		return;

	    setup_done = true;
	    int drw = getX11Drawable ();

	    ctrl.set_window (new java.lang.Integer (drw).toString ());
	}
    
    public Control (gnome.Bonobo.Control ctrl_)
	{
	    ctrl = ctrl_;
	    
	    setForeground (java.awt.Color.black);
	    setBackground (java.awt.Color.white);
	}
}
