/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.MonkeyBeans;

import java.util.*;
import gnome.Bonobo.UIHandlerPackage.*;

// Maps stringified menu paths to menu objects
public abstract class PathMenu
{
    Map items = new Hashtable ();
    
    static String remove_underscores (String label)
	{
	    int underscore_index = label.indexOf ("_");
	    
	    if (underscore_index != -1)
	    {
		return label.substring (0, underscore_index) +
		    label.substring (underscore_index + 1);
	    } else
		return label;
	}
    
    public void add (PathMenuListener listener,
		     String path,
		     MenuType type,
		     String label,
		     int pos)
	throws PathNotFound
	{
	    // Strip leading '/'
	    if (path.charAt (0) == '/')
		path = path.substring (1);
	    
	    int parent_index = path.indexOf ("/");

	    if (parent_index != -1)
	    {
		// Strip the first element of the menu path
		String child_name = path.substring (0, parent_index);
		path = path.substring (parent_index + 1);
		
		// Find the appropriate submenu
		Menu submenu = (Menu) items.get (child_name);
		if (submenu == null)
		    throw new PathNotFound ();
		submenu.add (listener, path, type, label, pos);
		
	    } else {
		
		// Do the actual submenu creation
		add_item (listener, path, type, label, pos);
	    }
	}

    public void remove_by_listener (PathMenuListener listener)
	{
	    // FIXME: Needs some thinking...
	}

    abstract void add_item (PathMenuListener listener,
			    String name,
			    MenuType type,
			    String label,
			    int pos);
}
