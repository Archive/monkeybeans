/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.MonkeyBeans;

import java.awt.event.*;
import gnome.Bonobo.*;

// Listener
public class PathMenuListener implements ActionListener
{
    UIHandler uihandler;
    
    public void actionPerformed (ActionEvent e)
	{
	    String path = e.getActionCommand ();
	    System.out.println ("***\t" + path);

	    uihandler.menu_activated (path);
	}

    PathMenuListener (UIHandler uihandler_)
	{
	    uihandler = uihandler_;
	}
}
