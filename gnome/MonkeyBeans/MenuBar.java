/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.MonkeyBeans;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import gnome.Bonobo.UIHandlerPackage.*;

public class MenuBar extends PathMenu
{
    java.awt.MenuBar awt_menu = new java.awt.MenuBar ();
    
    void add_submenu (String name,
		      String label,
		      int pos)
	{
	    label = remove_underscores (label);
	    Menu submenu = new Menu (label, "/" + name);
	    items.put (name, submenu);

	    awt_menu.add (submenu.awt_menu);
	}
    
    void add_item (PathMenuListener listener,
		   String name,
		   MenuType type,
		   String label,
		   int pos)
	{
	    if (type.value() == MenuType._MenuTypeSubtree)
	    {
		
		add_submenu (name, label, pos);
		
	    } else {

		// FIXME: Unimplemented
	    }
	}

    public MenuBar ()
	{
	}
}
