/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.MonkeyBeans.OAF;

import gnome.OAF.*;
import gnome.MonkeyBeans.*;
import java.io.*;
import gnu.getopt.*;

public class ObjectRegistrator implements gnome.MonkeyBeans.ObjectRegistrator
{
    org.omg.CORBA.ORB orb;
    ActivationContext ac;
    ObjectDirectory od;
    int fd = 0;

    String od_ior = null, ior_fd = null, iid = null;

    public void register_factory (String iid,
				  gnome.GNOME.ObjectFactory factory)
	{
	    od.lock ();
	    od.register_new (iid, factory);
	    od.unlock ();
	    
	    OAF.print_to_fd (fd, orb.object_to_string (factory));
	}

    public ObjectRegistrator (org.omg.CORBA.ORB orb_,
			      String[] argv)
	{
	    orb = orb_;
	    parse_argv (argv);

	    try {
		org.omg.CORBA.Object od_obj = orb.string_to_object (od_ior);
		od = ObjectDirectoryHelper.narrow (od_obj);

		fd = java.lang.Integer.parseInt (ior_fd);
	    } catch (Exception e) {
		od = null;
	    }
	}


    void parse_argv (String[] argv)
	{
	    final int OAF_OD_IOR = 0;
	    final int OAF_IOR_FD = 1;
	    final int OAF_IID    = 2;;
	    final int OAF_PRIV   = 3;
	    
	    int c;
	    String arg;
	    LongOpt[] longopts = new LongOpt[4];
	    
	    longopts[0] = new LongOpt ("oaf-od-ior",
				       LongOpt.REQUIRED_ARGUMENT, null,
				       OAF_OD_IOR);
	    longopts[1] = new LongOpt ("oaf-ior-fd",
				       LongOpt.REQUIRED_ARGUMENT, null,
				       OAF_IOR_FD);
	    longopts[2] = new LongOpt ("oaf-activate-iid",
				       LongOpt.REQUIRED_ARGUMENT, null,
				       OAF_IID);
	    longopts[3] = new LongOpt ("oaf-private",
				       LongOpt.NO_ARGUMENT, null,
				       OAF_PRIV);

	    Getopt g = new Getopt("OAF Registration", argv, "", longopts);

	    while ((c = g.getopt()) != -1)
	    {
		arg = g.getOptarg ();
		
		switch (c)
		{
		case OAF_OD_IOR:
		    od_ior = arg;
		    break;
		case OAF_IOR_FD:
		    ior_fd = arg;
		    break;
		case OAF_IID:
		    iid = arg;
		    break;
		case OAF_PRIV:
		    System.err.println ("Private registration not yet supported");
		    break;
		}
	    }
	}
    
}
