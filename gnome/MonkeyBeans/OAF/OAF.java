package gnome.MonkeyBeans.OAF;

import gnome.OAF.*;
import gnome.MonkeyBeans.*;
import java.io.*;

public class OAF
{
    public static native void print_to_fd (int fd,
					   String text);

    static // Class initializer
	{
	    System.loadLibrary ("monkeybeans");
	}
    
    public static org.omg.CORBA.Context create_context (org.omg.CORBA.ORB orb)
	{
	    org.omg.CORBA.Context ctx = orb.get_default_context ();
	    org.omg.CORBA.Any val = orb.create_any ();
	    
	    val.insert_string ("localhost");
	    ctx.set_one_value ("hostname", val);
	    
	    val.insert_string (System.getProperty ("user.name"));
	    ctx.set_one_value ("username", val);
	    
	    val.insert_string ("user");
	    ctx.set_one_value ("domain", val);

	    val.insert_string ("localhost:0");
	    ctx.set_one_value ("display", val);
	    
	    return ctx;	    
	}
    
    public static ActivationContext get_ac (org.omg.CORBA.ORB orb)
	{
	    org.omg.CORBA.Object obj = null;
	    String ior = null;
	    
	    // Get the IOR
	    try {
		String filename = "/tmp/orbit-" + System.getProperty("user.name") +
		    "/reg.IDL:OAF_ActivationContext:1.0";
		BufferedReader br = new BufferedReader (new FileReader (filename));
		ior = br.readLine ();
	    } catch (IOException e)
	    {
		System.err.println ("Couldn't get OAF IOR");
	    }
	    
	    // Create a CORBA object from the IOR
	    obj = orb.string_to_object (ior);
	    
	    // Downcast it to an ActivationContext
	    return ActivationContextHelper.narrow (obj);
	}
}
