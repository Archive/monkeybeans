/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.MonkeyBeans.OAF;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import org.omg.CORBA.*;
import gnome.OAF.*;


class ObjectBrowserWindow extends Dialog
    implements ActionListener, ItemListener
{
    List l = new List (10);
    Button btnOK = new Button ("OK");
    Button btnCancel = new Button ("Cancel");
    TextArea description = new TextArea (3, 40);
    ServerInfo[] obj_list = null;
    int selected = -1;

    String get_serverinfo (ServerInfo si, String fieldname)
	{
	    Property[] props = si.props;
	    String retval = null;
	    
	    int i = 0;
	    while (i < props.length &&
		   !props[i].name.equals (fieldname))
		i++;
	    
	    if (i < props.length)
		retval = props[i].v.value_string ();

	    return retval;
	    
	}
    
    public ObjectBrowserWindow ()
	{
	    super (new Frame(), "Select component", true);

	    l.setSize (0, 150);
	    l.addItemListener (this);
	    add ("North", l);

	    description.setEditable (false);
	    add ("Center", description);
	    
	    Panel p = new Panel ();
	    btnOK.addActionListener (this);
	    p.add (btnOK);
	    btnCancel.addActionListener (this);
	    p.add (btnCancel);
	    add ("South", p);

	    pack ();
	    setLocation (100, 100);
	}
    
    public void fill (ServerInfo[] _obj_list)
	{
	    obj_list = _obj_list;
	    
	    for (int i = 0; i < obj_list.length; i++)
	    {
		String curr_name = get_serverinfo (obj_list[i], "name");
		if (curr_name == null)
		{
		    curr_name = get_serverinfo (obj_list[i], "description");
		    if (curr_name == null)
			curr_name = obj_list[i].iid;
		}
		
		l.add (curr_name);
	    }
	}
    
    public ServerInfo get_object ()
	{
	    show ();
	    if (selected != -1)
		return obj_list[selected];
	    else
		return null;
	}
	
    public void actionPerformed (ActionEvent e)
	{
	    if (e.getSource().equals (btnOK))
		selected = l.getSelectedIndex ();
	    if (e.getSource().equals (btnCancel))
		selected = -1;
	    hide ();
	}

    public void itemStateChanged (ItemEvent e)
	{
	    ServerInfo curr_object = obj_list[l.getSelectedIndex ()];
	    String desc = get_serverinfo (curr_object, "description");
	    if (desc == null)
		desc = get_serverinfo (curr_object, "name");
	    
	    description.setText (desc);
	}
}

public class ObjectBrowser extends ObjectActivator
    implements gnome.MonkeyBeans.ObjectBrowser
{
    ObjectBrowserWindow wnd;
    
    public ObjectBrowser (org.omg.CORBA.ORB orb_)
	{
	    super (orb_);

	    wnd = new ObjectBrowserWindow ();
	}
    
    public String get_iid (String[] interfaces)
	{
	    try
	    {

		String[] query_commands = new String[interfaces.length];
		String query_command = "";
		
		for (int i = 0; i < interfaces.length; i++)
		{
		    query_commands[i] = "repo_ids.has('" +
			interfaces[i] +
			"')";
		}
		
		query_command = query_commands[0];
		
		for (int i = 1; i < query_commands.length; i++)
		{
		    query_command += " AND " + query_commands[i];
		}

		// Run our query
		ServerInfo[] obj_list = actx.query (query_command,
						    new String[]{},
						    orb.get_default_context());

		// Fill the object list
		wnd.fill (obj_list);
	    } catch (Exception e)
	    {
		System.err.println (e);
	    }
	    
	    ServerInfo obj = wnd.get_object ();
	    if (obj != null)
		return obj.iid;
	    else
		return null;
	}
}

