/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.MonkeyBeans.OAF;

import java.io.*;
import gnome.OAF.*;

class ObjectActivator implements gnome.MonkeyBeans.ObjectActivator
{
    org.omg.CORBA.ORB orb;
    ActivationContext actx;
    
    public org.omg.CORBA.Object activate_from_id (String id)
	{
	    ActivationResult res = null;
	    org.omg.CORBA.Object retval = null;

	    if (id == null)
		return null;

	    try {
		res = actx.activate_from_id (id, 0, OAF.create_context (orb));
	    } catch (org.omg.CORBA.UserException e)
	    {
	    }

	    if (res == null)
		return null;

	    switch (res.res.discriminator ().value ())
	    {
	    case ActivationResultType._RESULT_OBJECT:
		retval = res.res.res_object ();
		break;

	    case ActivationResultType._RESULT_SHLIB:
		System.err.println ("*** SHLIB activators not yet supported");

	    case ActivationResultType._RESULT_NONE:

	    default:
	    }

	    return retval;
	}
    
    public ObjectActivator (org.omg.CORBA.ORB orb_)
	{
	    orb = orb_;
	    actx = OAF.get_ac (orb);
	}
}

