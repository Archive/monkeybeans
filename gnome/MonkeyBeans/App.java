/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.MonkeyBeans;

import java.util.*;
import gnome.Bonobo.*;
import gnome.Bonobo.UIHandlerPackage.*;

public class App extends java.awt.Frame
{
    MenuBar mb = new MenuBar ();
    Map listeners = new Hashtable ();
    
    public void menu_create (UIHandler listener,
			     String path,
			     MenuType type,
			     String label,
			     int pos)
	throws PathNotFound
	{
	    mb.add (new PathMenuListener (listener), path, type, label, pos);

	    if (listener == null)
		return;
	    
	    List menuitems = (List) listeners.get (listener);
	    if (menuitems == null)
	    {
		menuitems = new LinkedList ();
		listeners.put (listener, menuitems);
	    }
	    menuitems.add (path);
	}
    
    public void unregister_containee (UIHandler listener)
	{
	    List menuitems = (List) listeners.get (listener);

	    if (menuitems == null)
		return;

	    Iterator i = menuitems.iterator ();
	    while (i.hasNext ())
	    {
		String path = (String) i.next ();
		System.out.println ("Would remove\t" + path);
	    }
	}

    public App (String title)
	{
	    super (title);

	    // Create stock menus
	    try
	    {
		menu_create (null, "/File",
			     MenuType.MenuTypeSubtree,
			     "_File", -1);
		menu_create (null, "/Edit",
			     MenuType.MenuTypeSubtree,
			     "_Edit", -1);
		menu_create (null, "/Help",
			 MenuType.MenuTypeSubtree,
			     "_Help", -1);
	    } catch (PathNotFound e) {
	    }

	    // Install the main menu bar
	    setMenuBar (mb.awt_menu);
	}
    
}
