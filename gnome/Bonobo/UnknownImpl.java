/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.Bonobo;

import org.omg.CORBA.*;
import java.util.*;

public class UnknownImpl implements UnknownOperations
{
    Set interfaces = new HashSet();
    UnknownImpl outer = null;
    int refcount = 0;

    /*
     * ref/unref: We'll just NOP it for now
     */
    public synchronized void ref ()
	{
	    refcount++;
	}

    public synchronized void unref ()
	{
	    refcount--;

	    if (refcount < 0)
	    {
		error ("Bonobo::UnknownImpl: Negative refcount reached!");
		refcount = 0;
	    }
	    
	    if (refcount == 0)
	    {
		// Die
		destroy ();
	    }
	}

    /*
     * query_interface:
     */
    public Unknown query_interface (String repo_id)
	{
	    if (outer != null)
		return outer.query_interface (repo_id);

	    if (tie ()._is_a (repo_id))
		return tie ();

	    Iterator i = interfaces.iterator();
	    while(i.hasNext())
	    {
		Unknown curr_obj = ((UnknownImpl) i.next ()).tie();
		if (curr_obj._is_a (repo_id))
		{
		    curr_obj.ref ();
		    return curr_obj;
		}
	    }
	    
	    return null;
	}

    void error (String message)
	{
	    System.err.println ("*** MonkeyBeans  ERROR  ***: " +
				this + ": " + message);
	}

    void warning (String message)
	{
	    System.err.println ("*** MonkeyBeans WARNING ***: " +
				this + ": " + message);
	}
    
    /*
     * add_interface: Merge new_iface's aggregates into the object
     */
    public void add_interface (UnknownImpl new_iface)
	{
	    if (refcount != 0)
	    {
		error ("Post-activation aggregation requested!");
	    }
	    
	    // Merge the two aggregates
	    interfaces.add (new_iface);
	    new_iface.outer = this;
	}

    org.omg.PortableServer.Servant tie;
    
    public Unknown tie ()
	{
	    return UnknownHelper.narrow (tie._this_object ());
	}
    
    public UnknownImpl (ORB orb)
	{
	    this ();
	    
	    tie = new UnknownPOATie (this);
	    tie._this_object (orb);
	}
    
    UnknownImpl ()
	{
	    refcount = 1;
	}

    void destroy ()
	{
	}
}
