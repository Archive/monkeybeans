/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.Bonobo;

import gnome.Bonobo.ContainerPackage.*;
import org.omg.CORBA.*;

public class ContainerImpl extends UnknownImpl implements ContainerOperations
{
    java.util.List clientsites = new java.util.LinkedList ();
    
    /**
     * enum_objects:
     * 
     * Returns: a list of the objects contained by this container
     */
    public Unknown[] enum_objects()
	{
	    Unknown[] ret_val = new Unknown [clientsites.size ()];
	    java.util.Iterator i = clientsites.iterator ();
	    int j = 0;

	    while (i.hasNext ())
		ret_val[j++] = ((UnknownImpl)i.next ()).tie ();
	    
	    return null;
	}
    
    /**
     * get_object:
     * @item_name : Item name to bind to
     * @only_if_exists : if TRUE, only bind if this object currently exists
     * otherwise, it will try to create the server for it.
     */
    public Unknown get_object(java.lang.String item_name, boolean only_if_exists)
	throws SyntaxError, NotFound
	{
	    System.out.println ("Container::get_object");
	    
	    return null;
	}

    public void add (UnknownImpl client_site)
	{
	    clientsites.add (client_site);
	}

    public void remove (UnknownImpl client_site)
	{
	    clientsites.remove (client_site);
	}
    
    public ContainerImpl (ORB orb)
	{
	    this ();

	    tie = new ContainerPOATie (this);
	    tie._this_object (orb);
	}

    ContainerImpl ()
	{
	    super ();
	}

    void destroy ()
	{
	    // Unref the ClientSites
	    java.util.Iterator i = clientsites.iterator ();

	    while (i.hasNext ())
		((UnknownImpl)i.next()).unref ();
	}
}
