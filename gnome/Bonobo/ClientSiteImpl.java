/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.Bonobo;

import org.omg.CORBA.*;

public class ClientSiteImpl extends UnknownImpl
    implements ClientSiteOperations

{
    ContainerImpl container = null;
    Embeddable embeddable = null;
    java.util.List viewframes = new java.util.LinkedList ();

    /**
     * get_container:
     *
     * Returns a pointer to the Container instance.
     */
    public Container get_container ()
	{
	    System.out.println ("get_container");
	    
	    container.ref ();
	    return ContainerHelper.narrow (container.tie ());
	}
    
    /**
     * show_window:
     * @show: boolean value, reports whether the
     * containee is showing its own separate editing window.
     *
     * Used by the containee to inform the parent process
     * when it shows its own separate editing window.
     *
     */
    public void show_window (boolean shown)
	{
	    System.out.println ("show_window");
	}
    
    /**
     * save_object:
     * 
     * Saves the object that is attached to this client site.
     * This operation is syncronous and will be done by the time
     * the function returns.
     */
    public gnome.Bonobo.PersistPackage.Status save_object ()
	{   
	    gnome.Bonobo.PersistPackage.Status retval;
	    
	    retval = gnome.Bonobo.PersistPackage.Status.SAVE_OK;
	    
	    return retval;
	}

    public void bind_embeddable (Embeddable embeddable_)
	{
	    if (embeddable != null)
	    {
		embeddable.unref ();
	    }
	    
	    embeddable = embeddable_;
	    embeddable.ref ();
	    
	    embeddable.set_client_site (ClientSiteHelper.narrow (tie ()));
	    
	}

    public ViewFrameImpl new_view (UIHandlerImpl uih)
	{
	    return new_view (UIHandlerHelper.narrow (uih.tie ()));
	}
    
    public ViewFrameImpl new_view (UIHandler uih)
	{
	    ViewFrameImpl frame = new ViewFrameImpl (tie._orb (),
						     ClientSiteHelper.narrow (tie ()),
						     uih);
	    
	    // Create view
	    View view = embeddable.new_view (ViewFrameHelper.narrow (frame.tie ()));
	    frame.bind_to_view (view);
	    view.unref ();
	    view._release ();

	    // Register this viewframe
	    viewframes.add (frame);
	    
	    return frame;
	}

    public ClientSiteImpl (ORB orb, ContainerImpl _container)
	{
	    this (_container);
	    
	    tie = new ClientSitePOATie (this);
	    tie._this_object (orb);
	}

    ClientSiteImpl (ContainerImpl _container)
	{
	    super ();
	    
	    container = _container;
	    container.add (this);
	}

    void destroy ()
	{
	    if (embeddable != null)
	    {
		embeddable.unref ();
		embeddable = null;
	    }
	}
}
