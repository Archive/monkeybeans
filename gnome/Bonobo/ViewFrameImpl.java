/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.Bonobo;

import org.omg.CORBA.*;

public class ViewFrameImpl extends ControlFrameImpl
    implements ViewFrameOperations
{
    ClientSite client_site;
    
    /**
     * get_client_site:
     * 
     * Returns: The Bonobo::ClientSite interface associated
     * with this ViewFrame.
     */
    public ClientSite get_client_site()
	{
	    System.out.println ("ViewFrame::get_client_site");

	    client_site.ref ();
	    return client_site;
	}

    View view;
    
    public void bind_to_view (View view_)
	{
	    view = view_;

	    view.ref ();
	}
    
    public View get_view ()
	{
	    return view;
	}

    ViewFrameImpl (ClientSite _client_site,
		   UIHandler _uihandler)
	{
	    super (_uihandler);
	    
	    client_site = _client_site;
	}

    public ViewFrameImpl (ORB orb,
			  ClientSiteImpl _client_site,
			  UIHandlerImpl _uihandler)
	{
	    this (orb,
		  ClientSiteHelper.narrow (_client_site.tie ()),
		  UIHandlerHelper.narrow (_uihandler.tie ()));
	}
    
    public ViewFrameImpl (ORB orb,
			  ClientSite _client_site,
			  UIHandler _uihandler)
	{
	    this (_client_site, _uihandler);
	    
	    tie = new ViewFramePOATie (this);
	    tie._this_object (orb);
	}

    void destroy ()
	{
	    if (view != null)
	    {
		view.unref ();
		view = null;
	    }
	}
}
