/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.Bonobo;

import org.omg.CORBA.*;

public class UIHandlerImpl extends UnknownImpl
    implements UIHandlerOperations
{
    /**
     * register_containee:
     * @uih : The UIHandler CORBA interface of the
     * containee which is being registered.
     * 
     * This method is used by an embedded component to register
     * its BonoboUIHandler with the container's BonoboUIHandler.
     */
    public void register_containee(UIHandler uih)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler
	{
	    System.out.println ("UIHandler::register_containee");
	}
    
    /**
     * unregister_containee:
     * @uih : The UIHandler of the containee which is being
     * unregistered
     * 
     * This method is used by an embedded component to unregister
     * its BonoboUIHandler with the container's BonoboUIHandler.
     * The container will then
     */
    public void unregister_containee(UIHandler uih)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler
	{
	    System.out.println ("UIHandler::unregister_containee");
	    app.unregister_containee (uih);
	}
    
    /**
     * 
     */
    public UIHandler get_toplevel()
	{
	    System.out.println ("UIHandler::get_toplevel");

	    ref ();
	    return UIHandlerHelper.narrow (tie ());
	}
    
    /**
     * menu_create:
     */
    public void menu_create (UIHandler containee,
			     String path,
			     gnome.Bonobo.UIHandlerPackage.MenuType type,
			     String label,
			     String hint,
			     int pos,
			     gnome.Bonobo.UIHandlerPackage.PixmapType pixmap_type,
			     byte[] pixmap_data,
			     int accelerator_key,
			     int modifier)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	    System.out.print ("UIHandler::menu_create:\t\t");
	    System.out.println (path + "\t" + label);


	    app.menu_create (containee, path, type, label, pos);
	    
	}
    
    /**
     * menu_remove:
     */
    public void menu_remove (UIHandler containee_uih,
			     String path)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	    System.out.print ("UIHandler::menu_remove:\t");
	    System.out.println (path);
	}
    
    /**
     * menu_fetch:
     */
    public void menu_fetch (String path,
			    gnome.Bonobo.UIHandlerPackage.MenuTypeHolder type,
			    org.omg.CORBA.StringHolder label,
			    org.omg.CORBA.StringHolder hint,
			    org.omg.CORBA.IntHolder pos,
			    gnome.Bonobo.UIHandlerPackage.PixmapTypeHolder pixmap_type,
			    gnome.Bonobo.UIHandlerPackage.iobufHolder pixmap_data,
			    org.omg.CORBA.IntHolder accelerator_key,
			    org.omg.CORBA.IntHolder modifier)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}
    
    /**
     * menu_get_children:
     */
    public void menu_get_children (String path,
				   gnome.Bonobo.UIHandlerPackage.StringSeqHolder children)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}
    
    /**
     * menu_set_attributes: small data entries.
     */
    public void menu_set_attributes (UIHandler containee,
				     String path,
				     boolean sensitivity,
				     int pos,
				     String label,
				     String hint,
				     int accelerator_key,
				     int ac_mods,
				     boolean toggle_state)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}
    
    /**
     * menu_get_attributes: small data entries.
     */
    public void menu_get_attributes (UIHandler containee,
				     String path,
				     org.omg.CORBA.BooleanHolder sensitivity,
				     org.omg.CORBA.IntHolder pos,
				     org.omg.CORBA.StringHolder label,
				     org.omg.CORBA.StringHolder hint,
				     org.omg.CORBA.IntHolder accelerator_key,
				     org.omg.CORBA.IntHolder ac_mods,
				     org.omg.CORBA.BooleanHolder toggle_state)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	    System.out.print ("UIHandler::menu_get_attributes:\t");
	    System.out.println (path);

	    // Not implemented yet -- throw an exception
	    throw new gnome.Bonobo.UIHandlerPackage.PathNotFound ();
	}
    
    /**
     * menu_set_data: chunky data entries.
     */
    public void menu_set_data (UIHandler containee,
			       String path,
			       gnome.Bonobo.UIHandlerPackage.PixmapType pixmap_type,
			       byte[] pixmap_data)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}
    
    /**
     * menu_get_data: chunky data entries.
     */
    public void menu_get_data (UIHandler containee,
			       String path,
			       gnome.Bonobo.UIHandlerPackage.PixmapTypeHolder pixmap_type,
			       gnome.Bonobo.UIHandlerPackage.iobufHolder pixmap_data)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}
    
    public void toolbar_create (UIHandler containee,
				String name)
	throws gnome.Bonobo.UIHandlerPackage.NotToplevelHandler
	{
	    System.out.println ("UIHandler::toolbar_create\t" + name);
	}

    public void toolbar_remove (UIHandler containee,
				String name)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	    System.out.println ("UIHandler::toolbar_remove\t" + name);
	}
    
    public void toolbar_get_children (String path,
				      gnome.Bonobo.UIHandlerPackage.StringSeqHolder children)
	throws gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	    System.out.println ("UIHandler::toolbar_get_children");
	}

    public void toolbar_set_attributes (UIHandler containee,
					String name,
					gnome.Bonobo.UIHandlerPackage.ToolbarOrientation orientation,
					gnome.Bonobo.UIHandlerPackage.ToolbarStyle style,
					gnome.Bonobo.UIHandlerPackage.ToolbarSpaceStyle space_style,
					gnome.Bonobo.UIHandlerPackage.ReliefStyle relief_style,
					int space_size,
					boolean sensitive)
	throws gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}

    public void toolbar_get_attributes (UIHandler containee,
					String name,
					gnome.Bonobo.UIHandlerPackage.ToolbarOrientationHolder orientation,
					gnome.Bonobo.UIHandlerPackage.ToolbarStyleHolder style,
					gnome.Bonobo.UIHandlerPackage.ToolbarSpaceStyleHolder space_style,
					gnome.Bonobo.UIHandlerPackage.ReliefStyleHolder relief_style,
					org.omg.CORBA.IntHolder space_size,
					org.omg.CORBA.BooleanHolder sensitive)
	throws gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}

    public void toolbar_create_item (UIHandler containee,
				     String path,
				     gnome.Bonobo.UIHandlerPackage.ToolbarType type,
				     String label,
				     String hint,
				     int pos,
				     Control control,
				     gnome.Bonobo.UIHandlerPackage.PixmapType pixmap_type,
				     byte[] pixmap_data,
				     int accelerator_key,
				     int modifier)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	    System.out.println ("UIHandler::toolbar_create_item:\t" +
				path);
	    
	    gnome.MonkeyBeans.Pixmap pm = new
		gnome.MonkeyBeans.Pixmap (pixmap_data);
	}
    
    public void toolbar_remove_item (UIHandler containee,
				     String path)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}
    
    public void toolbar_fetch_item (String path,
				    gnome.Bonobo.UIHandlerPackage.ToolbarTypeHolder type,
				    org.omg.CORBA.StringHolder label,
				    org.omg.CORBA.StringHolder hint,
				    org.omg.CORBA.IntHolder pos,
				    gnome.Bonobo.UIHandlerPackage.PixmapTypeHolder pixmap_type,
				    gnome.Bonobo.UIHandlerPackage.iobufHolder pixmap_data,
				    org.omg.CORBA.IntHolder accelerator_key,
				    org.omg.CORBA.IntHolder modifier)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}
    
    public void toolbar_item_set_attributes (UIHandler containee,
					     String path,
					     boolean sensitive,
					     boolean active,
					     int pos,
					     String label,
					     String hint,
					     int accelerator_key,
					     int ac_mods,
					     boolean toggle_state)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}
    
    public void toolbar_item_get_attributes (UIHandler containee,
					     String path,
					     org.omg.CORBA.BooleanHolder sensitive,
					     org.omg.CORBA.BooleanHolder active,
					     org.omg.CORBA.IntHolder pos,
					     org.omg.CORBA.StringHolder label,
					     org.omg.CORBA.StringHolder hint,
					     org.omg.CORBA.IntHolder accelerator_key,
					     org.omg.CORBA.IntHolder ac_mods,
					     org.omg.CORBA.BooleanHolder toggle_state)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}

    public void toolbar_item_set_data (UIHandler containee,
				       String path,
				       gnome.Bonobo.UIHandlerPackage.PixmapType pixmap_type,
				       byte[] pixmap_data)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}

    public void toolbar_item_get_data (UIHandler containee,
				       String path,
				       gnome.Bonobo.UIHandlerPackage.PixmapTypeHolder pixmap_type,
				       gnome.Bonobo.UIHandlerPackage.iobufHolder pixmap_data)
	throws
	gnome.Bonobo.UIHandlerPackage.NotToplevelHandler,
	gnome.Bonobo.UIHandlerPackage.PathNotFound
	{
	}

    public void menu_activated (String path)
	{
	}
    
    public void menu_removed (String path)
	{
	}
    
    public void menu_overridden (String path)
	{
	}

    public void menu_reinstated (String path)
	{
	}

    public void toolbar_activated (String path)
	{
	}

    public void toolbar_removed (String path)
	{
	}

    public void toolbar_overridden (String path)
	{
	}

    public void toolbar_reinstated (String path)
	{
	}

    public boolean dock_add( UIHandler containee,
			     String name,
			     Control control,
			     int behavior,
			     gnome.Bonobo.UIHandlerPackage.DockPlacement placement,
			     int band_num,
			     int band_position,
			     int offset)
	{
	    System.out.println ("UIHandler::dock_add");
	    return true;
	}
    
    public boolean dock_remove(UIHandler containee,
			       String name)
	{
	    System.out.println ("UIHandler::dock_remove");
	    return true;
	}

    public boolean dock_set_sensitive(UIHandler containee,
				      String name,
				      boolean sensitivity)
	{
	    System.out.print ("UIHandler::dock_set_sensitive");
	    return true;
	}
    
    public boolean dock_get_sensitive(UIHandler containee,
				      String name)
	{
	    System.out.println ("UIHandler::dock_get_sensitive");
	    return true;
	}


    gnome.MonkeyBeans.App app;
    
    
    public UIHandlerImpl (gnome.MonkeyBeans.App app_)
	{
	    super ();

	    app = app_;
	}

    public UIHandlerImpl ()
	{
	    super ();
	}
    
    public UIHandlerImpl (ORB orb,
			  gnome.MonkeyBeans.App app_)
	{
	    this (app_);
	    
	    tie = new UIHandlerPOATie (this);
	    tie._this_object (orb);
	}
}
