/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.Bonobo;

import org.omg.CORBA.*;

public class EmbeddableImpl extends UnknownImpl
    implements EmbeddableOperations
{
    ClientSite client_site = null;
    ViewFactory view_factory = null;
    java.util.List views = new java.util.LinkedList ();
    
    public void set_client_site(ClientSite client_site_)
	{
	    System.out.println ("Embeddable::get_client_site");

	    /*
	    if (client_site != null)
		client_site.unref ();
	    */
	    
	    client_site = client_site_;
	    //client_site.ref ();
	}

    public ClientSite get_client_site()
	{
	    System.out.println ("Embeddable::get_client_site");

	    return client_site;
	}

    public void set_host_name(String name,
			      String appname)
	{
	}

    public void set_uri(String uri)
	{
	}

    public void close(gnome.Bonobo.EmbeddablePackage.CloseMode mode)
        throws gnome.Bonobo.EmbeddablePackage.UserCancelledSave
	{
	}

    public gnome.Bonobo.EmbeddablePackage.GnomeVerb[] get_verb_list()
	{
	    System.out.println ("Embeddable::get_verb_list");
	    return null;
	}

    public void advise(AdviseSink advise)
	{
	}

    public void unadvise()
	{
	}

    public int get_misc_status(int type)
	{
	    return 0;
	}

    public View new_view(ViewFrame frame)
        {
	    System.out.println ("Embeddable::new_view");

	    ViewImpl view = view_factory.new_view (this, frame);

	    if (view == null)
		{
		    warning ("ViewFactory failed");
		    return null;
		}
	    
	    view.set_frame (frame);
//	    view.set_embeddable (this);
	    
	    views.add (view);
	    
	    return ViewHelper.narrow (view.tie ());
	}

    public gnome.Bonobo.Canvas.Component new_canvas_item(boolean aa,
							 gnome.Bonobo.Canvas.ComponentProxy proxy)
        {
	    return null;
	}
    
    EmbeddableImpl (ViewFactory view_factory_)
	{
	    super ();

	    view_factory = view_factory_;
	    if (view_factory == null)
	    {
		error ("Invalid ViewFactory");
	    }
	}

    public EmbeddableImpl (ORB orb, ViewFactory view_factory_)
	{
	    this (view_factory_);
	    
	    tie = new EmbeddablePOATie (this);
	    tie._this_object (orb);
	}
}
