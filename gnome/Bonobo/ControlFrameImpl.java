/* $Id$ */
/*
  MonkeyBeans Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package gnome.Bonobo;

import org.omg.CORBA.*;

public class ControlFrameImpl extends UnknownImpl
    implements ControlFrameOperations
{
    /**
     * activated:
     * @state: TRUE if the associated Control has been activated,
     * FALSE if it just became inactive.
     */
    public void activated (boolean state)
	{
	    System.out.println ("ControlFrame::activated");
	}
    
    /**
     * get_ambient_properties:
     * 
     * Returns: A PropertyBag containing the ambient properties
     * for this container.
     */
    public PropertyBag get_ambient_properties ()
	{
	    System.out.println ("ControlFrame::get_ambient_properties");
	    return null;
	}
    
    /** 
     * queue_resize:
     *
     * Tell the container that the Control would like to be
     * resized.  It should follow-up by calling
     * Control:size_request()
     *
     */
    public void queue_resize ()
	{
	    System.out.println ("ControlFrame::queue_resize");
	}
    
    /**
     * activate_uri:
     * @uri: The uri we would like to get loaded.
     *
     * This is used by the containee when it needs
     * to ask the container to perform some action
     * with a given URI.
     *
     * The user has requested that the uri be loaded
     */
    public void activate_uri (String uri, boolean relative)
	{
	    System.out.println ("ControlFrame::activate_uri");
	}
    
    /**
     * get_ui_handler:
     *
     * Returns: The Bonobo::UIHandler interface to be used for
     * menu/toolbar item merging.
     */
    public UIHandler get_ui_handler ()
	{
	    System.out.println ("ControlFrame::get_ui_handler");

	    uihandler.ref ();
	    return uihandler;
	}
    
    /**
     * deactivate_and_undo:
     *
     * This is called by the active object if it has just been activated
     * and the undo operation has been called
     */
    public void deactivate_and_undo ()
	{
	    System.out.println ("ControlFrame::deactivate_and_undo");
	}


    UIHandler uihandler;
    
    
    ControlFrameImpl (UIHandler _uihandler)
	{
	    super ();
	    
	    uihandler = _uihandler;
	}

    public ControlFrameImpl (ORB orb,
			     UIHandlerImpl _uihandler)
	{
	    this (orb,
		  UIHandlerHelper.narrow (_uihandler.tie ()));
	}
    
    public ControlFrameImpl (ORB orb,
			     UIHandler _uihandler)
	{
	    this (_uihandler);
	    
	    tie = new ControlFramePOATie (this);
	    tie._this_object (orb);
	}
}
