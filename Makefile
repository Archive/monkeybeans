include		Make.vars

IDLDIR		= $(shell $(GNOME_CONFIG) --datadir)/idl
IDLS		= Bonobo.idl oaf.idl
ROOT		= .

JIDLFLAGS	+= --tie --all -I$(IDLDIR) --package gnome

IDLFILES	= $(patsubst %, $(IDLDIR)/%, $(IDLS))
IDLSTAMPS	= $(patsubst %.idl, jidl-stamp.%, $(IDLS))

DIRS		= gnome

SRCS		= $(wildcard *.java)
CLASSES		= $(patsubst %.java, %.class, $(SRCS))

all: $(IDLSTAMPS)

jidl-stamp.%: $(IDLDIR)/%.idl
	$(JIDL) $(JIDLFLAGS) $<
	touch $@

include		Make.rules
